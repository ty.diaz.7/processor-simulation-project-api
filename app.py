from flask import Flask, request
import math
from datetime import datetime
from decimal import Decimal

app = Flask(__name__)

today = datetime.today() #today's date in a datetime object

@app.route('/request', methods=['POST'])
def req_validate(): 
    filename = request.get_json() #Retrieves json file from POST request
    
    amount = filename['Amount'] 
    exp = filename['Expiration'] 
    acc = filename['Account']

    acc = int(acc)
    exp = datetime.strptime(exp,"%m/%y") #gets the expiration date as a datetime object for comparison
    amount = Decimal(amount) #setting to Decimal should eradicate any floating point errors in large transactions
    first_four = acc // 10 ** (int(math.log(acc, 10)) - 4 + 1) #gets the first 4 digits of a number
    length = int(math.log10(acc))+1 #gets the length of the account number without needing to convert to a string

    if exp >= today:
        if 0 < amount <= Decimal(99999.99):
            if 4000 <= first_four <= 4999 and 13 <= length <=19: #check if the first four digits and total length are within the correct bounds for Visa
                return "Visa card found. Transaction approved.", 200
            elif 5000 <= first_four <= 5999 and length == 16: #same as above
                return "MASTERCARD card found. Transaction approved.", 200
            elif 6000 <= first_four <= 6599 and 16 <= length <= 19: #''
                return "DISCOVER card found. Transaction approved.", 200
            elif 3400 <= first_four <= 3799 and length == 15: #''
                return "AMEX card found. Transaction approved.", 200
            else:
                return "Invalid card number.", 403
        else:
            return "Transaction amount invalid.", 403
    else:
        return "Card expired.", 403

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=105)
