## Information
Python 3.7.3,
Flask 1.1.1,
Werkzeug 0.15.4

### To Run
Verify that Python 3 and Flask are installed; if Python is not in your PATH environment you will need to use the python launcher 'py' instead of python itself.
```
'flask --version'
'python --version' or 'py --version'
```
if Python is not installed, you can get the latest version here `https://www.python.org/downloads/windows/` then use `pip install flask` or `py -m pip install flask` to install any missing Flask dependencies

to run the program use:
```
'cd ..\processor-simulation-project-api-main' (or whatever you saved the folder as)
'python app.py' or 'py app.py'
```

## Supplemental Information
I have never attempted anything regarding HTTP requests before, so this was a wonderful way to immerse myself in the environment and flesh things out on my own. While the code is certainly forceful, it does perform the functions required of it in a way that relatively easy to follow and can be expanded. I chose to use Python for this project, eschewing the 3 preferred languages, because it has a lot of powerful built in functions and it's the language I'm most familiar with. I also recognize Python is not the easiest to distribute as it's an interpretted language and not one that's usually compiled. This isn't meant to be obstinate; I have no issues learning and using other languages. 

### Take Aways
There was no shortage of things I learned by doing this, and they're worth mentioning to fully understand my thought process. Overall, this project took me around 8 hours to complete. I used two algorithms with the math library to both count the length of "Account" and find the first 4 digits, I felt that this was a way to shorten the complexity as opposed to slicing strings. Again, HTTP requests are completely new to me and I found the experience extremely valuable, as I learned a fair bit about how information is returned to the client and Status Code Definitions for various responses such as 200 OK, 201 Created, 400 Bad Request, and 403 Forbidden.
